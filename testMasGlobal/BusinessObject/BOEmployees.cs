﻿using System;
using testMasGlobal.DataObject;
using testMasGlobal.DataAccess;
using testMasGlobal.DataTransferObject;
using Newtonsoft.Json;
using System.Dynamic;

namespace testMasGlobal.BusinessObject
{
    public class BOEmployees
    {
        public dynamic getEmployees(int idEmployee)
        {
            EmployeeAnnualSalary[] employeeResponse =new EmployeeAnnualSalary[0];
            dynamic[] employeesDyn;
            EmployeesSource employeesSource = new DataAccess.EmployeesSource();
            String responseDA;
            responseDA= employeesSource.GetEmployees();
            employeesDyn= JsonConvert.DeserializeObject<ExpandoObject[]>(responseDA);
            employeeResponse = new EmployeeAnnualSalary[employeesDyn.Length];
            for (int i = 0; i < employeesDyn.Length; i++)
            {
                if (idEmployee != -1) //-1 read all
                {
                    employeeResponse = new EmployeeAnnualSalary[1];
                    if (employeesDyn[i].id == idEmployee)
                    {
                        EmployeeAnnualSalary employeeAnnualSalary = new EmployeeAnnualSalary( 
                            new DataObject.Employee(
                            (int) (employeesDyn[i].id==null?-1: employeesDyn[i].id), 
                            (string)(employeesDyn[i].name == null ? "": employeesDyn[i].name),
                            (string)(employeesDyn[i].contractTypeName == null ? "" : employeesDyn[i].contractTypeName),
                            (int) (employeesDyn[i].roleId == null ? -1 : employeesDyn[i].roleId),
                            (string)(employeesDyn[i].roleName == null ? "" : employeesDyn[i].roleName),
                            (string)(employeesDyn[i].roleDescription == null ? "" : employeesDyn[i].roleDescription),
                            (Double)(employeesDyn[i].hourlySalary == null ? 0 : employeesDyn[i].hourlySalary),
                            (Double)(employeesDyn[i].monthlySalary == null ? 0 : employeesDyn[i].monthlySalary)
                            ));
                        employeeResponse[0] = employeeAnnualSalary;
                        break;
                    }
                }
                else
                {
                    EmployeeAnnualSalary employeeAnnualSalary = new EmployeeAnnualSalary(
                           new DataObject.Employee(
                            (int)(employeesDyn[i].id == null ? -1 : employeesDyn[i].id),
                            (string)(employeesDyn[i].name == null ? "" : employeesDyn[i].name),
                            (string)(employeesDyn[i].contractTypeName == null ? "" : employeesDyn[i].contractTypeName),
                            (int)(employeesDyn[i].roleId == null ? -1 : employeesDyn[i].roleId),
                            (string)(employeesDyn[i].roleName == null ? "" : employeesDyn[i].roleName),
                            (string)(employeesDyn[i].roleDescription == null ? "" : employeesDyn[i].roleDescription),
                            (Double)(employeesDyn[i].hourlySalary == null ? 0 : employeesDyn[i].hourlySalary),
                            (Double)(employeesDyn[i].monthlySalary == null ? 0 : employeesDyn[i].monthlySalary)
                           ));
                    employeeResponse[i] = employeeAnnualSalary;
                }
            }
            return employeeResponse;
        }
    }
}
