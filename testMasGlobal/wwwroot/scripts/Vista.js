
$(document).ready(function () {
   
   
    //inicializeGrid(null);

    //var $table = $('#dgvEmployee');
    var $table = $('#dgvEmployee');
    $table.bootstrapTable();
})

function inicializeGrid(dataEmp) {
    var $table = $('#dgvEmployee');
    $table.bootstrapTable('load', dataEmp );
    //var datagrid = $('#dgvEmployee').bootstrapTable({ data: dataEmp});
}
function callbackEmployeeQry(EmployeeRet) {
    dataRes = [];
    for (i = 0; i < EmployeeRet.length; i++) {
        employee = EmployeeRet[i]
        dataRes[i] = {
            "id": employee.id,
            "name": employee.name,
            "contract_type_name": employee.contractTypeName,
            "role_id": employee.roleId,
            "role_name": employee.roleName,
            "role_description": employee.roleDescription,
            "hourly_salary":  Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(employee.hourlySalary), 
            "monthly_salary":  Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(employee.monthlySalary), 
            "annual_salary":  Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(employee.annualSalary), 
        }
    }
    inicializeGrid(dataRes);
   
}
function getEmployee() {
    idEmployee = document.getElementById("impIdEmployee").value;
    getDatosSvr(idEmployee, callbackEmployeeQry);
}
Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
