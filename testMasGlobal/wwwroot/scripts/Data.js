
var rootService = location.protocol + "//" + location.host;
function getDatosSvr(IdEmployee,CallBackEmployeeQuery){
    $.ajax({
        url: rootService + "/testMasGlobal/api/Employee/idEmployee/" + IdEmployee,
        type:"GET",
        dataType:"json",
        contentType: "application/json, charset=utf-8",
        statusCode: {
            200: function (respuesta) {
                    CallBackEmployeeQuery( respuesta)
            },
            401: function (respuesta) {
                console.log(respuesta);
            },
            500: function (respuesta) {
                console.log(respuesta);
            }
        }
    })
}


