﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace testMasGlobal.DataObject
{
    public class Employee
    {
        private int _id;
        private string _name;
        private string _contractTypeName;
        private int _roleId;
        private string _roleName;
        private string _roleDescription;
        private double _hourlySalary;
        private double _monthlySalary;

        public Employee()
        {
        }

        public Employee(global::System.Int32 id=-1, global::System.String name="", global::System.String contractTypeName="", 
            global::System.Int32 roleId=-1, global::System.String roleName="", global::System.String roleDescription="", 
            global::System.Double hourlySalary=0, global::System.Double monthlySalary=0)
        {
            _id = id;
            _name = name;
            _contractTypeName = contractTypeName;
            _roleId = roleId;
            _roleName = roleName;
            _roleDescription = roleDescription;
            _hourlySalary = hourlySalary;
            _monthlySalary = monthlySalary;
           
        }

        public global::System.Int32 Id { get => _id; set => _id = value; }
        public global::System.String Name { get => _name; set => _name = value; }
        public global::System.Int32 RoleId { get => _roleId; set => _roleId = value; }
        public global::System.String RoleName { get => _roleName; set => _roleName = value; }
        public global::System.String RoleDescription { get => _roleDescription; set => _roleDescription = value; }
        public global::System.Double HourlySalary { get => _hourlySalary; set => _hourlySalary = value; }
        public global::System.Double MonthlySalary { get => _monthlySalary; set => _monthlySalary = value; }
        public global::System.String ContractTypeName { get => _contractTypeName; set => _contractTypeName = value; }
    }
}
