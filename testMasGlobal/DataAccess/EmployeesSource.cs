﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace testMasGlobal.DataAccess
{
    public class EmployeesSource
    {

        public string GetEmployees()
        {
            HttpWebRequest myReq;
            HttpWebResponse myResp;
            string myText = "";
            try
            {
                myReq = (HttpWebRequest)WebRequest.Create(new Uri("http://masglobaltestapi.azurewebsites.net/api/Employees"));
                myReq.Method = "GET";
                myReq.ContentType = "application/json";
                //myReq.GetRequestStream().Write(Encoding.UTF8.GetBytes(myData), 0, System.Text.Encoding.UTF8.GetBytes(myData).Count());
                myResp = (HttpWebResponse)myReq.GetResponse();
                System.IO.StreamReader myreader = new System.IO.StreamReader(myResp.GetResponseStream());

                myText = myreader.ReadToEnd();
                Console.WriteLine(myText);
            }
            catch (Exception ex)
            {
                myText = ex.Message;
            }

            return myText;
        }
    }
}
