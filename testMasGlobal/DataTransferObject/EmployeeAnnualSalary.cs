﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using testMasGlobal.DataObject;
namespace testMasGlobal.DataTransferObject
{
    public class EmployeeAnnualSalary : Employee 
    {

    private double _annualSalary;

        public EmployeeAnnualSalary(Employee employee)
        {
            //this = employee;
            this.Id = employee.Id;
            this.Name = employee.Name;
            this.ContractTypeName = employee.ContractTypeName;
            this.RoleId = employee.RoleId;
            this.RoleName = employee.RoleName;
            this.RoleDescription = employee.RoleDescription;
            this.HourlySalary = employee.HourlySalary;
            this.MonthlySalary = employee.MonthlySalary;


            switch (this.ContractTypeName)
            {
                case "HourlySalaryEmployee":
                    _annualSalary = this.HourlySalary * 12 * 120;
                     break;
                case "MonthlySalaryEmployee":
                    _annualSalary = this.MonthlySalary * 12;
                    break;
            }
        }
        public global::System.Double AnnualSalary { get => _annualSalary; set => _annualSalary = value; }
    }
}
