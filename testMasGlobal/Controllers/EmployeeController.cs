﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using testMasGlobal.BusinessObject;
using testMasGlobal.DataObject;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace testMasGlobal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : Controller
    {
        // GET api/values
        [HttpGet("idEmployee/{idEmployee}/", Name = "EmployeQuery")]
        public IActionResult EmployeQuery(string idEmployee)
        {
            int defaultVal = 0;
            dynamic respuesta;
            BOEmployees boEmployee = new BOEmployees();
            setHeader();
            if(String.IsNullOrEmpty(idEmployee))
            {
                idEmployee = "-1";
            }
            respuesta = boEmployee.getEmployees(Int32.Parse(idEmployee));
            return Ok(respuesta);
        }
        // GET api/values
        [HttpGet("idEmployee/", Name = "EmployeQueryNoParam")]
        public IActionResult EmployeQueryNoParam()
        {
            int defaultVal = 0;
            dynamic respuesta;
            BOEmployees boEmployee = new BOEmployees();
            setHeader();
           
            respuesta = boEmployee.getEmployees(-1);
            return Ok(respuesta);
        }

        void setHeader()
        {
        #if (DEBUG)
            {
                Response.Headers.SetCommaSeparatedValues("Access-Control-Allow-Origin", "http://localhost");
            }
        #else
            {
                Response.Headers.SetCommaSeparatedValues("Access-Control-Allow-Origin","http://localhost");
            }
        #endif
            Response.Headers.SetCommaSeparatedValues("Content-Type", "application/json");
            Response.Headers.SetCommaSeparatedValues("Accept", "application/json");
            Response.Headers.SetCommaSeparatedValues("Access-Control-Allow-Headers", "Origin", "X-Requested-With", "Content-Type", "Accept");
            Response.Headers.SetCommaSeparatedValues("Access-Control-Allow-Methods", "GET", "POST", "PUT", "DELETE");
            Response.Headers.Add("X-Content-Type-Options", "nosniff");
        }
    }

}
